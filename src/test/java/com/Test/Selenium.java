package com.Test;

import org.testng.annotations.Test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Selenium {
	WebDriver driver;

	@BeforeMethod

	public void setUp() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		driver.get("https://admin-demo.nopcommerce.com/login");
		WebElement uname = driver.findElement(By.id("Email"));
		uname.clear();
		uname.sendKeys("admin@yourstore.com");
		WebElement pwd = driver.findElement(By.id("Password"));
		pwd.clear();
		pwd.sendKeys("admin");
		driver.findElement(By.xpath("//button[@type='submit']")).click();

	}

	@Test(priority = 1)

	public void login() {

		String text = driver
				.findElement(By.xpath("//nav[@class='main-header navbar navbar-expand-md navbar-dark bg-dark']//li[2]"))
				.getText();
		Assert.assertEquals(text, "John Smith");

	}

	@Test(priority = 2, dataProvider = "getComputerInfo")
	public void categories(String name, String description) {
		driver.findElement(By.xpath("//ul[@class='nav nav-pills nav-sidebar flex-column nav-legacy']/child::li[2]/a"))
				.click();

		driver.findElement(By.xpath("//p[text()=' Categories']")).click();
		driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();
		driver.findElement(By.id("Name")).sendKeys(name);

		driver.switchTo().frame("Description_ifr");
		driver.findElement(By.id("tinymce")).sendKeys(description);
		driver.switchTo().defaultContent();
		WebElement parent = driver.findElement(By.id("ParentCategoryId"));
		Select s = new Select(parent);
		s.selectByIndex(3);
		driver.findElement(By.name("save")).click();
		driver.findElement(By.xpath("//div[text()='Search']"));
		driver.findElement(By.name("SearchCategoryName")).sendKeys("Build your own computer");
		driver.findElement(By.id("search-categories")).click();
		String text = driver
				.findElement(By.xpath(
						"//tr[@class='odd']//td[contains(text(),'Computers >> Desktops >> Build your own computer')]"))
				.getText();
		Assert.assertEquals(text, "Computers >> Desktops >> Build your own computer");

	}

	@Test(priority = 3)

	public void product() {
		driver.findElement(By.xpath("//ul[@class='nav nav-pills nav-sidebar flex-column nav-legacy']/child::li[2]/a"))
				.click();
		driver.findElement(By.xpath("//p[normalize-space()='Products']")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		driver.findElement(By.id("search-products")).click();
		String productName = driver.findElement(By.xpath("//td[normalize-space()='Build your own computer']"))
				.getText();
		Assert.assertEquals(productName, "Build your own computer");

	}

	@Test(priority = 4, dataProvider = "getManufactureInfo")
	public void manfacturers(String name, String description) {
		driver.findElement(By.xpath("//ul[@class='nav nav-pills nav-sidebar flex-column nav-legacy']/child::li[2]/a"))
				.click();
		driver.findElement(By.xpath("//p[normalize-space()='Manufacturers']")).click();
		driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();
		driver.findElement(By.id("Name")).sendKeys(name);
		driver.switchTo().frame("Description_ifr");
		driver.findElement(By.id("tinymce")).sendKeys(description);
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//button[@name='save']")).click();
		driver.findElement(By.id("SearchManufacturerName")).sendKeys("Apple");
		driver.findElement(By.id("search-manufacturers")).click();
		String mobile = driver.findElement(By.xpath("//td[normalize-space()='Apple']")).getText();
		Assert.assertEquals(mobile, "Apple");

	}

	@Test(priority = 5)

	public void logOut() {

		driver.findElement(By.xpath("//a[normalize-space()='Logout']")).click();
		Assert.assertEquals(driver.getTitle(), "Your store. Login");
	}

	@AfterMethod

	public void tearDown() {

		driver.quit();
	}

	@DataProvider

	public Object[][] getManufactureInfo() throws IOException {

		Object[][] data1 = Data.getTestData1();
		return data1;
	}

	@DataProvider
	public Object[][] getComputerInfo() throws IOException {
		Object[][] data = Data.getTestData();

		return data;
	}

}